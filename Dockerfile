FROM node:alpine

USER node
WORKDIR /home/node
ENV PATH=/home/node/.yarn/bin:$PATH
RUN yarn global add web-ext
